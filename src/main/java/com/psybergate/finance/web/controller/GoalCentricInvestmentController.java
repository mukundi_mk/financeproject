package com.psybergate.finance.web.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Transaction;
import com.psybergate.finance.domain.customer.Customer;
import com.psybergate.finance.domain.investment.GoalCentricInvestment;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.service.CustomerService;
import com.psybergate.finance.service.GoalCentricInvestmentService;
import com.psybergate.finance.web.controller.util.Pages;
import com.psybergate.finance.web.controller.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;

public class GoalCentricInvestmentController implements Serializable {

  private static final long serialVersionUID = 1L;

  private GoalCentricInvestmentService investmentService;

  private CustomerService customerService;
  
  private EventController eventController;

  private TransactionController transactionController;

  private GoalCentricInvestment investment;

  private Customer customer;
  
  private Transaction transaction;
  
  private Event event;

  @Autowired
  public GoalCentricInvestmentController(GoalCentricInvestmentService investmentService, CustomerService customerService) {
    this.investmentService = investmentService;
    this.customerService = customerService;
  }

  public GoalCentricInvestmentController() {
  }

  @PostConstruct
  private void init() {
    investment = (GoalCentricInvestment) RequestUtil.getObject("goalInvestment");
    customer =  (Customer) RequestUtil.getObject("customer");
    transaction = new Transaction();
    event = new Event();
  }

  public void deleteInvestment(GoalCentricInvestment investment) {
    investmentService.remove(investment);
  }

  public void addGoalCentricInvestmentInvestmentForm(Customer customer) {
    RequestUtil.putObject("customer",customer);
    RequestUtil.putObject("goalInvestment",new GoalCentricInvestment());
  }


  public void setGoalCentricInvestment(GoalCentricInvestment investment) {
    this.investment = investment;
  }

  public void saveGoalCentricInvestmentForecastUpdate() {
    investmentService.updateInvestment(investment);
    RequestUtil.displayMessage("Updated Forecast saved", "Forecast has been saved successfully...");
  }
  
  public String addGoalCentricInvestmentForecast(Customer customer) {
    investment.setCustomer(customer);
    investmentService.generateForecastDetails(investment);
    RequestUtil.putObject("goalInvestment",this.investment);
    return Pages.GoalCentricInvestment.DISPLAY;
  }
  
  public void editInvestmentForecast() {
    investmentService.generateForecastDetails(investment);
  }
  
  public void saveGoalCentricInvestment() {
    if (!(customerService.getAllGoalCentricInvestment(investment.getCustomer()).contains(investment))) {
      investmentService.save(investment);
      RequestUtil.displayMessage("Forecast saved", "Forecast has been saved successfully...");
    } else {
      saveGoalCentricInvestmentForecastUpdate();
    }
  }
  
  public String viewForecast(GoalCentricInvestment investment) {
    investment = (GoalCentricInvestment) investmentService.get(investment.getId());
    investmentService.generateForecastDetails(investment);
    RequestUtil.putObject("goalInvestment", investment);
    return Pages.GoalCentricInvestment.DISPLAY;
  }
  
  public void addTransaction() {
    transaction.setForecast(investment);
    transactionController.addTransaction(transaction);
    transaction = new Transaction();
    editInvestmentForecast();
  }


  public void editTransaction() {
    editInvestmentForecast();
  }

  public void deleteTransaction(Transaction transaction) {
    transactionController.remove(transaction);
    editInvestmentForecast();
  }
  
  public void addEvent() {
    event.setForecast(investment);
    eventController.addEvent(event);
    event = new Event();
    editInvestmentForecast();
  }
  
  public void editEvent() {
    editInvestmentForecast();
  }

  public void deleteEvent(Event event) {
    eventController.remove(event);
    editInvestmentForecast();
  }

  public void setInvestment(GoalCentricInvestment investment) {
    this.investment = investment;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public GoalCentricInvestmentService getInvestmentService() {
    return investmentService;
  }

  public void setInvestmentService(GoalCentricInvestmentService investmentService) {
    this.investmentService = investmentService;
  }

  public Investment getInvestment() {
    return investment;
  }

  public Event getEvent() {
    return event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public void setTransaction(Transaction transaction) {
    this.transaction = transaction;
  }


}
