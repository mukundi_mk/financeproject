package com.psybergate.finance.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;


import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.customer.Customer;
import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.GoalCentricInvestment;
import com.psybergate.finance.service.CustomerService;
import com.psybergate.finance.web.controller.util.Pages;
import com.psybergate.finance.web.controller.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;


public class CustomerController
    implements Serializable {

  private static final long serialVersionUID = 1L;

  private CustomerService customerService;

  private Customer customer;

  private List<Customer> customers;

  private List<Bond> bondList;

  private List<BasicInvestment> basicInvestmentList;
  
  private List<GoalCentricInvestment> goalCentricInvestmentList;

  @Autowired
  public CustomerController(CustomerService customerService) {
    this.customerService = customerService;
  }

  @PostConstruct
  public void init() {
    customer =  (Customer) RequestUtil.getObject("customer");
    customers = new ArrayList<>();
  }
  
  public void getAllBondForCustomer(Customer customer) {
    bondList = customerService.getAllBonds(customer);
    RequestUtil.putObject("bondList",bondList);
  }

  public void getAllBasicInvestmentForCustomer(Customer customer) {
    basicInvestmentList = customerService.getAllBasicInvestment(customer);
    RequestUtil.putObject("basicInvestmentList",basicInvestmentList);
  }
  
  public void getAllGoalCentricInvestmentForCustomer(Customer customer) {
    goalCentricInvestmentList = customerService.getAllGoalCentricInvestment(customer);
    RequestUtil.putObject("goalCentricInvestmentList",goalCentricInvestmentList);
  }

  public void deleteCustomer(Customer customer) {
    customerService.remove(customer);
  }

  public String addCustomerForm() {
    RequestUtil.putObject("customer", new Customer());
    return Pages.Customer.REGISTER;
  }

  public String index() {
    return Pages.HOME;
  }

  public String addCustomer() {
    customerService.registerCustomer(customer);
    RequestUtil.putObject("customer", new Customer());
    return Pages.HOME;
  }

  public String forecast(Customer customer) {
    getAllBondForCustomer(customer);
    getAllBasicInvestmentForCustomer(customer);
    getAllGoalCentricInvestmentForCustomer(customer);
    RequestUtil.putObject("customer",customer);
    return Pages.Customer.HISTORY;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public List<Customer> getCustomers() {
    if (customers != null)
      customers = customerService.getAllCustomers();
    return customers;
  }

  public void setCustomers(List<Customer> customers) {
    this.customers = customers;
  }

  public List<Bond> getBondList() {
    return (List<Bond>) RequestUtil.getObject("bondList");
  }

  public void setBondList(List<Bond> bondList) {
    this.bondList = bondList;
  }

  public CustomerService getCustomerService() {
    return customerService;
  }

  public void setCustomerService(CustomerService customerService) {
    this.customerService = customerService;
  }

  public List<BasicInvestment> getBasicInvestmentList() {
    return (List<BasicInvestment>) RequestUtil.getObject("basicInvestmentList");
  }

  public void setBasicInvestmentList(List<BasicInvestment> basicInvestmentList) {
    this.basicInvestmentList = basicInvestmentList;
  }

  public List<GoalCentricInvestment> getGoalCentricInvestmentList() {
    return (List<GoalCentricInvestment>) RequestUtil.getObject("goalCentricInvestmentList");
  }

  public void setGoalCentricInvestmentList(List<GoalCentricInvestment> goalCentricInvestmentList) {
    this.goalCentricInvestmentList = goalCentricInvestmentList;
  }

  


}
