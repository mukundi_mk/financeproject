package com.psybergate.finance.web.controller;

import java.io.Serializable;

import com.psybergate.finance.domain.Transaction;




public class TransactionController implements Serializable{

  private static final long serialVersionUID = 1L;

  public void addTransaction(Transaction transaction) {
    transaction.getForecast().getTransactions().add(transaction);
  }

  public void remove(Transaction transaction) {
    transaction.getForecast().getTransactions().remove(transaction);
  }

}
