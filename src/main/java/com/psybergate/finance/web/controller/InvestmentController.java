package com.psybergate.finance.web.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;


import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Transaction;
import com.psybergate.finance.domain.customer.Customer;
import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.service.CustomerService;
import com.psybergate.finance.service.InvestmentService;
import com.psybergate.finance.web.controller.util.Pages;
import com.psybergate.finance.web.controller.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;


public class InvestmentController implements Serializable {

  private static final long serialVersionUID = 1L;

  private InvestmentService investmentService;
  
  private CustomerService customerService;
  
  private EventController eventController;

  private TransactionController transactionController;
  
  private BasicInvestment investment;
  
  private Customer customer;
  
  private Transaction transaction;
  
  private Event event;
  
  @Autowired
  public InvestmentController(InvestmentService investmentService, CustomerService customerService,EventController eventController,TransactionController transactionController) {
    this.investmentService = investmentService;
    this.customerService = customerService;
    this.eventController = eventController;
    this.transactionController = transactionController;
  }
  
  public InvestmentController() {
  }

  @PostConstruct
  private void init() {
    investment = (BasicInvestment) RequestUtil.getObject("investment");
    customer =  (Customer) RequestUtil.getObject("customer");
    transaction = new Transaction();
    event = new Event();
  }

  public void deleteInvestment(BasicInvestment investment) {
    investmentService.remove(investment);
  }

  public String addInvestmentForm(Customer customer) {
    RequestUtil.putObject("customer",customer);
    RequestUtil.putObject("investment",new BasicInvestment());
    return Pages.Investment.CREATE;

  }
  
  public String addInvestmentForecast(Customer customer) {
    investment.setCustomer(customer);
    investmentService.generateForecastDetails(investment);
    RequestUtil.putObject("investment",this.investment);
    return Pages.Investment.DISPLAY;
  }
  
  public void editInvestmentForecast() {
    investmentService.generateForecastDetails(investment);
  }
  
  public void saveInvestmentForecastUpdate() {
    investmentService.updateInvestment(investment);
    RequestUtil.displayMessage("Updated forecast saved", "Forecast has been saved successfully...");
  }

  public void saveInvestment() {
    if (!(customerService.getAllBasicInvestment(investment.getCustomer()).contains(investment))) {
      investmentService.save(investment);
      RequestUtil.displayMessage("Forecast saved", "Forecast has been saved successfully...");
    } else {
      saveInvestmentForecastUpdate();
    }
  }
  
  public String viewForecast(BasicInvestment investment) {
    investment = (BasicInvestment) investmentService.get(investment.getId());
    investmentService.generateForecastDetails(investment);
    RequestUtil.putObject("investment", investment);
    return Pages.Investment.DISPLAY;
  }
  
  public void addTransaction() {
    transaction.setForecast(investment);
    transactionController.addTransaction(transaction);
    transaction = new Transaction();
    editInvestmentForecast();
  }


  public void editTransaction() {
    editInvestmentForecast();
  }

  public void deleteTransaction(Transaction transaction) {
    transactionController.remove(transaction);
    editInvestmentForecast();
  }
  
  public void addEvent() {
    event.setForecast(investment);
    eventController.addEvent(event);
    event = new Event();
    editInvestmentForecast();
  }
  
  public void editEvent() {
    editInvestmentForecast();
  }

  public void deleteEvent(Event event) {
    eventController.remove(event);
    editInvestmentForecast();
  }

  public void setInvestment(BasicInvestment investment) {
    this.investment = investment;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public InvestmentService getInvestmentService() {
    return investmentService;
  }

  public void setInvestmentService(InvestmentService investmentService) {
    this.investmentService = investmentService;
  }

  public Investment getInvestment() {
    return investment;
  }

  public Event getEvent() {
    return event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public void setTransaction(Transaction transaction) {
    this.transaction = transaction;
  }

}
