package com.psybergate.finance.web.controller;


import com.psybergate.finance.domain.Event;

import java.io.Serializable;


public class EventController implements Serializable{

  private static final long serialVersionUID = 1L;

  public void addEvent(Event event) {
    event.getForecast().getEvents().add(event);
  }

  public void remove(Event event) {
    System.out.println("here ec");
    event.getForecast().getEvents().remove(event);
  }

}
