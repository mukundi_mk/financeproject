package com.psybergate.finance.web.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;


import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Transaction;
import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.customer.Customer;
import com.psybergate.finance.service.BondService;
import com.psybergate.finance.service.CustomerService;
import com.psybergate.finance.web.controller.util.Pages;
import com.psybergate.finance.web.controller.util.RequestUtil;
import org.springframework.beans.factory.annotation.Autowired;


public class BondController
    implements Serializable {

  private static final long serialVersionUID = 1L;

  private BondService bondService;

  private CustomerService customerService;
  
  private EventController eventController;
  
  private TransactionController transactionController;

  private Bond bond;

  private Customer customer;
  
  private Transaction transaction;
  
  private Event event;

@Autowired
  public BondController(BondService bondService, CustomerService customerService,EventController eventController,TransactionController transactionController) {
    this.bondService = bondService;
    this.customerService = customerService;
    this.eventController = eventController;
    this.transactionController = transactionController;
  }
  
  public BondController() {
  }

  @PostConstruct
  private void init() {
    bond = (Bond) RequestUtil.getObject("bond");
    customer =  (Customer) RequestUtil.getObject("customer");
    transaction = new Transaction();
    event = new Event();
  }

  public void deleteBond(Bond bond) {
    bondService.remove(bond);
  }

  public String addBondForm(Customer customer) {
    RequestUtil.putObject("customer",customer);
    RequestUtil.putObject("bond",new Bond());
    return Pages.Bond.CREATE;
  }

  public String addBond(Customer customer) {
    bond.setCustomer(customer);
    bondService.generateForecastDetails(bond);
    RequestUtil.putObject("bond",this.bond);
    return Pages.Bond.DISPLAY;
  }

  public void editBondForecast() {
    bondService.generateForecastDetails(bond);
    RequestUtil.putObject("bond", bond);
  }
  
  public void saveBondForecastUpdate() {
    bondService.updateBond(bond);
    RequestUtil.displayMessage("Updated forecast saved", "Forecast has been saved successfully...");
  }

  public void saveBond() {
    if (!(customerService.getAllBonds(bond.getCustomer()).contains(bond))) {
      bondService.save(bond);
      RequestUtil.displayMessage("Forecast saved ", "Forecast has been saved successfully...");
    }
    else {
       saveBondForecastUpdate();
    }
  }

  public String viewForecast(Bond bond) {
    bond = (Bond) bondService.get(bond.getId());
    bondService.generateForecastDetails(bond);
    RequestUtil.putObject("bond", bond);
    return Pages.Bond.DISPLAY;
  }
  
  public void addTransaction() {
    transaction.setForecast(bond);
    transactionController.addTransaction(transaction);
    transaction = new Transaction();
    editBondForecast();
  }
  
  public void editTransaction() {
    editBondForecast();
  }
  
  public void deleteTransaction(Transaction transaction) {
    transactionController.remove(transaction);
    editBondForecast();
  }

  public void addEvent() {
    event.setForecast(bond);
    eventController.addEvent(event);
    event = new Event();
    editBondForecast();
  }
  
  public void editEvent() {
     editBondForecast();
  }

  public void deleteEvent(Event event) {
    eventController.remove(event);
    editBondForecast();
  }

  public Bond getBond() {
    return bond;
  }

  public void setBond(Bond bond) {
    this.bond = bond;
  }


  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Transaction getTransaction() {
    return transaction;
  }

  public void setTransaction(Transaction transaction) {
    this.transaction = transaction;
  }

  public Event getEvent() {
    return event;
  }

  public void setEvent(Event event) {
    this.event = event;
  }


}
