package com.psybergate.finance.web.controller.util;

public interface Pages {
  
  public static final String HOME = "index.xhtml";
  public static final String ABOUT = "about.xhtml";
  
  interface Customer{
    public static final String REGISTER = "registerCustomer.xhtml";
    public static final String HISTORY = "forecastHistory.xhtml";
    
  }
  
  interface Investment{
    public static final String CREATE = "createInvestmentForecast.xhtml";
    public static final String DISPLAY = "displayInvestmentForecast.xhtml";
    
  }
  interface GoalCentricInvestment{
    public static final String DISPLAY = "displayGoalCentricForecast.xhtml";
    
  }
  interface Bond{
    public static final String CREATE = "createBondForecast.xhtml";
    public static final String DISPLAY = "displayBondForecast.xhtml";
    
  }
  
}
