package com.psybergate.finance.resource;

import com.psybergate.finance.domain.investment.BasicInvestment;

public interface InvestmentResource {

  public void persist(BasicInvestment investment);

  public BasicInvestment get(long id);

  public BasicInvestment get(String forecastName);

  public void update(BasicInvestment investment);

  public void remove(BasicInvestment investment);
}
