package com.psybergate.finance.resource;

import java.util.List;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Transaction;

public interface EventResource {

  public void persist(List<Event> events);
  public void update(List<Event> events);
}
