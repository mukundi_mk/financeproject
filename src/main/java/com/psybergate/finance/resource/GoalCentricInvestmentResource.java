package com.psybergate.finance.resource;

import com.psybergate.finance.domain.investment.GoalCentricInvestment;

public interface GoalCentricInvestmentResource {

  public void persist(GoalCentricInvestment investment);

  public GoalCentricInvestment get(long id);
  
  public GoalCentricInvestment get(String forecastName);
  
  public void update(GoalCentricInvestment investment);
  
  public void remove(GoalCentricInvestment investment);
}
