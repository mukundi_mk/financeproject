
package com.psybergate.finance.resource;

import java.util.List;

import com.psybergate.finance.domain.Transaction;

public interface TransactionResource {
  public void persist(List<Transaction> transactions);
  public void update(List<Transaction> transactions);
}
