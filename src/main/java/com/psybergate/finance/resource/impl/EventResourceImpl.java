package com.psybergate.finance.resource.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.psybergate.finance.domain.Event;
import com.psybergate.finance.domain.Transaction;
import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.resource.EventResource;

public class EventResourceImpl implements EventResource{

  @PersistenceContext(name = "finance_project")
  private EntityManager entityManager;

  @Override
  public void persist(List<Event> events) {
    for (Event event : events) {
      entityManager.persist(event);
    }
  }
  
  @Override
  public void update(List<Event> events) {
    for (Event event : events) {
      event.setUpdateAuditInfo();
      entityManager.merge(event);
    }
  }

}
