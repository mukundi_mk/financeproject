package com.psybergate.finance.resource.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.customer.Customer;
import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.GoalCentricInvestment;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.resource.CustomerResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CustomerResourceImpl
    implements CustomerResource {

  @PersistenceContext(unitName = "finance_project")
  private EntityManager entityManager;

  public void persist(Customer customer) {
    entityManager.persist(customer);
  }

  public Customer get(Long customerID) {
    return entityManager.find(Customer.class, customerID);
  }

  public void remove(Customer customer) {
    entityManager
        .remove(entityManager.contains(customer) ? customer : entityManager.merge(customer));
  }

  @SuppressWarnings("unchecked")
  public List<Customer> getAll() {

    return entityManager.createNativeQuery("select * from customer;", Customer.class)
        .getResultList();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<Bond> getAllBonds(Customer customer) {
    return entityManager
        .createNativeQuery("select * from bond where customer_id=" + customer.getId() + ";",
            Bond.class)
        .getResultList();
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<BasicInvestment> getAllBasicInvestment(Customer customer) {
    List<BasicInvestment> basicInvestments = entityManager
        .createNativeQuery("select * from basicinvestment where customer_id=" + customer.getId() + ";",
            BasicInvestment.class)
        .getResultList();
    
    return basicInvestments;
  }

  @SuppressWarnings("unchecked")
  @Override
  public List<GoalCentricInvestment> getAllGoalCentricInvestment(Customer customer) {
    
    List<GoalCentricInvestment> goalCentricInvestments = entityManager
        .createNativeQuery("select * from goalcentricinvestment where customer_id=" + customer.getId() + ";",
            GoalCentricInvestment.class)
        .getResultList();
    
    return goalCentricInvestments;
  }

}
