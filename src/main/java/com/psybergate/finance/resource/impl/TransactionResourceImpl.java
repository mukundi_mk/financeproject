package com.psybergate.finance.resource.impl;

import com.psybergate.finance.domain.Transaction;
import com.psybergate.finance.resource.TransactionResource;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository
public class TransactionResourceImpl implements TransactionResource {
  @PersistenceContext(name = "finance_project")
  private EntityManager entityManager;

  @Override
  public void persist(List<Transaction> transactions) {
    for (Transaction transaction : transactions) {
      entityManager.persist(transaction);
    }
  }


  @Override
  public void update(List<Transaction> transactions) {
    for (Transaction transaction : transactions) {
      transaction.setUpdateAuditInfo();
      entityManager.merge(transaction);
    }
  }

}
