package com.psybergate.finance.resource.impl;

import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.resource.BondResource;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class BondResourceImpl
    implements BondResource {

  @PersistenceContext(unitName = "finance_project")
  private EntityManager entityManager;

  public void persist(Bond bond) {
    entityManager.persist(bond);
  }

  public Bond get(long bondId) {
    return entityManager.find(Bond.class, bondId);
  }

  @Override
  public void update(Bond bond) {
    Bond oldBond = get(bond.getForecastName());
    bond.setId(oldBond.getId());
    bond.setCustomer(oldBond.getCustomer());
    bond.setUpdateAuditInfo();
    entityManager.merge(bond);
  }

  @Override
  public Bond get(String forecastName) {
    Query query = entityManager.createNamedQuery("Bond.findByForecastName", Bond.class);
    query.setParameter("forecastName", forecastName);
    List<Bond> bonds = query.getResultList();
    return bonds.get(0);
  }

  @Override
  public void remove(Bond bond) {
    entityManager.remove(entityManager.contains(bond) ? bond : entityManager.merge(bond));
  }


}
