package com.psybergate.finance.resource.impl;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.psybergate.finance.domain.investment.GoalCentricInvestment;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.resource.GoalCentricInvestmentResource;
import org.springframework.stereotype.Repository;

@Repository
public class GoalCentricInvestmentResourceImpl implements GoalCentricInvestmentResource{

  @PersistenceContext(name = "finance_project")
  private EntityManager entityManager;

  @Override
  public void persist(GoalCentricInvestment investment) {
    entityManager.merge(investment.getCustomer());
    entityManager.persist(investment);
  }

  @Override
  public GoalCentricInvestment get(long id) {
    return entityManager.find(GoalCentricInvestment.class, id);
  }

  @SuppressWarnings("unchecked")
  public List<Investment> getAll(Long id) {
    return entityManager.createNativeQuery("select * from goalcentricinvestment where customer_id=" + id + ";",
        GoalCentricInvestment.class).getResultList();
  }

  @Override
  public void update(GoalCentricInvestment investment) {
    Investment oldInvestment = get(investment.getForecastName());
    investment.setId(oldInvestment.getId());
    investment.setCustomer(oldInvestment.getCustomer());
    investment.setUpdateAuditInfo();
    entityManager.merge(investment);
    
  }

  @Override
  public GoalCentricInvestment get(String forecastName) {
    Query query = entityManager.createNamedQuery("GoalCentricInvestment.findByForecastName", GoalCentricInvestment.class);
    query.setParameter("forecastName", forecastName);
    List<GoalCentricInvestment> investments = query.getResultList();
    return investments.get(0);
  }

  @Override
  public void remove(GoalCentricInvestment investment) {
    entityManager.remove(entityManager.contains(investment) ? investment : entityManager.merge(investment));
  }

}
