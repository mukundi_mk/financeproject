package com.psybergate.finance.resource.impl;

import java.util.List;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.resource.InvestmentResource;
import org.springframework.stereotype.Repository;


@Repository
public class InvestmentResourceImpl implements InvestmentResource {

  @PersistenceContext(name = "finance_project")
  private EntityManager entityManager;

  @Override
  public void persist(BasicInvestment investment) {
    entityManager.persist(investment);
  }

  @Override
  public BasicInvestment get(long id) {
    return entityManager.find(BasicInvestment.class, id);
  }

  @SuppressWarnings("unchecked")
  public List<Investment> getAll(Long id) {
    return entityManager.createNativeQuery("select * from basicinvestment where customernum=" + id + ";",
        BasicInvestment.class).getResultList();
  }

  @Override
  public void update(BasicInvestment investment) {
    BasicInvestment oldInvestment = get(investment.getForecastName());
    investment.setId(oldInvestment.getId());
    investment.setCustomer(oldInvestment.getCustomer());
    investment.setUpdateAuditInfo();
    entityManager.merge(investment);
    
  }

  @Override
  public BasicInvestment get(String forecastName) {
    Query query = entityManager.createNamedQuery("Investment.findByForecastName", BasicInvestment.class);
    query.setParameter("forecastName", forecastName);
    List<BasicInvestment> investments = query.getResultList();
    return investments.get(0);
  }

  @Override
  public void remove(BasicInvestment investment) {
    entityManager.remove(entityManager.contains(investment) ? investment : entityManager.merge(investment));
  }

}
