package com.psybergate.finance.resource;

import java.util.List;

import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.customer.Customer;

public interface BondResource {

  public void persist(Bond bond);

  public Bond get(long bondId);
  
  public Bond get(String forecastName);

  
  public void update(Bond bond);

  public void remove(Bond bond);
}
