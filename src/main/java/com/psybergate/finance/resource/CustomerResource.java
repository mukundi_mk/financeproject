package com.psybergate.finance.resource;

import java.util.List;

import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.customer.Customer;
import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.GoalCentricInvestment;

public interface CustomerResource {

  public void persist(Customer customer);

  public Customer get(Long id);

  public void remove(Customer customer);

  public List<Customer> getAll();

  public List<Bond> getAllBonds(Customer customer);

  public List<BasicInvestment> getAllBasicInvestment(Customer customer);
  
  public List<GoalCentricInvestment> getAllGoalCentricInvestment(Customer customer);
  

}