package com.psybergate.finance.exceptions;

public class InvalidTermException extends RuntimeException{

  private static final long serialVersionUID = 1L;

  public InvalidTermException() {
    super();
  }

  public InvalidTermException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public InvalidTermException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidTermException(String message) {
    super(message);
  }

  public InvalidTermException(Throwable cause) {
    super(cause);
  }
  
}
