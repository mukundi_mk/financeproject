package com.psybergate.finance.exceptions;

public class MonthlyDepositException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public MonthlyDepositException() {
		super();
	}

	public MonthlyDepositException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MonthlyDepositException(String message, Throwable cause) {
		super(message, cause);
	}

	public MonthlyDepositException(String message) {
		super(message);
	}

	public MonthlyDepositException(Throwable cause) {
		super(cause);
	}

}
