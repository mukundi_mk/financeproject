package com.psybergate.finance.exceptions;

public class InvalidAmountException extends RuntimeException{

  private static final long serialVersionUID = 1L;

  public InvalidAmountException() {
    super();
  }

  public InvalidAmountException(String message, Throwable cause, boolean enableSuppression,
      boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }

  public InvalidAmountException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidAmountException(String message) {
    super(message);
  }

  public InvalidAmountException(Throwable cause) {
    super(cause);
  }

  
}
