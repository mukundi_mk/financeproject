package com.psybergate.finance.service;

import java.util.List;

import com.psybergate.finance.domain.Event;

public interface EventService {

  public void save(List<Event> events);
  public void update(List<Event> events);
}
