package com.psybergate.finance.service;

import java.util.List;

import com.psybergate.finance.domain.Transaction;

public interface TransactionService {
  public void save(List<Transaction> transactions);
  public void update(List<Transaction> transactions );
}
