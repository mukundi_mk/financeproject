package com.psybergate.finance.service.impl;

import java.util.List;


import com.psybergate.finance.domain.Transaction;
import com.psybergate.finance.resource.TransactionResource;
import com.psybergate.finance.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceImpl implements TransactionService {
  
  private TransactionResource bondTransactionResource;
  
  
  @Autowired
  public TransactionServiceImpl(TransactionResource bondTransactionResource) {
    this.bondTransactionResource = bondTransactionResource;
  }



  @Override
  public void save(List<Transaction> transactions) {
    bondTransactionResource.persist(transactions);
  }



  @Override
  public void update(List<Transaction> transactions) {
    bondTransactionResource.update(transactions);
  }

}
