package com.psybergate.finance.service.impl;



import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.resource.BondResource;
import com.psybergate.finance.service.BondService;
import com.psybergate.finance.service.EventService;
import com.psybergate.finance.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BondServiceImpl implements BondService {

  private BondResource bondResource;
  private TransactionService transactionService;
  private EventService eventService;

  public BondServiceImpl() {
  }

  @Autowired
  public BondServiceImpl(BondResource bondResource, TransactionService transactionService, EventService eventService) {
    this.bondResource = bondResource;
    this.transactionService = transactionService;
    this.eventService = eventService;
  }

  @Override
  public void save(Bond bond) {
    bondResource.persist(bond);
    transactionService.save(bond.getTransactions());
    eventService.save(bond.getEvents());
    generateForecastDetails(bond);
  }

  @Override
  public Bond get(long id) {
    Bond bond = bondResource.get(id);;
    bond.getEvents().size();
    bond.getTransactions().size();
    return bond;
  }

  @Override
  public void generateForecastDetails(Bond bond) {
    bond.initialiseBond();
  }

  @Override
  public Bond get(String forecastName) {
    
    return null;
  }

  @Override
  public void updateBond(Bond bond) {
    bondResource.update(bond);
    transactionService.update(bond.getTransactions());
    eventService.update(bond.getEvents());
    generateForecastDetails(bond);
  }

  @Override
  public void remove(Bond bond) {
    bondResource.remove(bond);
  }

}
