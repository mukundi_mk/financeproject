package com.psybergate.finance.service.impl;

import java.util.List;



import com.psybergate.finance.domain.Event;
import com.psybergate.finance.resource.EventResource;
import com.psybergate.finance.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventServiceImpl implements EventService{

  private EventResource bondEventResource;

  @Autowired
  public EventServiceImpl(EventResource bondEventResource) {
    this.bondEventResource = bondEventResource;
  }

  @Override
  public void save(List<Event> events) {
    
    bondEventResource.persist(events);
  }

  @Override
  public void update(List<Event> events) {
    bondEventResource.update(events);
  }
  
}
