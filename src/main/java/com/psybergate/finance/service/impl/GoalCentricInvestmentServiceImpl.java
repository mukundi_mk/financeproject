package com.psybergate.finance.service.impl;



import com.psybergate.finance.domain.investment.GoalCentricInvestment;
import com.psybergate.finance.resource.GoalCentricInvestmentResource;
import com.psybergate.finance.service.EventService;
import com.psybergate.finance.service.GoalCentricInvestmentService;
import com.psybergate.finance.service.TransactionService;
import com.sun.istack.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class GoalCentricInvestmentServiceImpl implements GoalCentricInvestmentService {

 static Logger logger =  Logger.getLogger(InvestmentServiceImpl.class);
  
  private GoalCentricInvestmentResource investmentResource;
  private TransactionService transactionService;
  private EventService eventService;

  public GoalCentricInvestmentServiceImpl() {
  }

  @Autowired
  public GoalCentricInvestmentServiceImpl(GoalCentricInvestmentResource investmentResource, TransactionService transactionService,  EventService eventService) {
    this.investmentResource = investmentResource;
    this.transactionService = transactionService;
    this.eventService = eventService;
  }

  @Override
  public void save(GoalCentricInvestment investment) {
    investmentResource.persist(investment);
    generateForecastDetails(investment);
  }

  @Override
  public GoalCentricInvestment get(long id) {
    GoalCentricInvestment investment = investmentResource.get(id);
    investment.getEvents().size();
    investment.getTransactions().size();
    return investment;
  }

  @Override
  public void generateForecastDetails(GoalCentricInvestment investment) {
    investment.initialiseInvestment();
  }

  @Override
  public void updateInvestment(GoalCentricInvestment investment) {
    investmentResource.update(investment);
    transactionService.update(investment.getTransactions());
    eventService.update(investment.getEvents());
    generateForecastDetails(investment);
  }

  @Override
  public GoalCentricInvestment get(String forecastName) {
    
    return investmentResource.get(forecastName);
  }

  @Override
  public void remove(GoalCentricInvestment investment) {
    investmentResource.remove(investment);
  }

}
