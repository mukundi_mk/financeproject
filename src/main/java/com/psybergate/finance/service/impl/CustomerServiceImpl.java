package com.psybergate.finance.service.impl;

import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.customer.Customer;
import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.GoalCentricInvestment;
import com.psybergate.finance.resource.CustomerResource;
import com.psybergate.finance.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl
    implements CustomerService {

  private CustomerResource customerResource;

  @Autowired
  public CustomerServiceImpl(CustomerResource customerResource) {
    this.customerResource = customerResource;
  }

  public void registerCustomer(Customer cust) {

    customerResource.persist(cust);
  }

  public Customer getCustomer(Long id) {

    return customerResource.get(id);

  }

  public List<Customer> getAllCustomers() {

    return customerResource.getAll();
  }

  @Override
  public void remove(Customer customer) {
    customerResource.remove(customer);
  }

  @Override
  public List<Bond> getAllBonds(Customer customer) {

    return customerResource.getAllBonds(customer);
  }

  @Override
  public List<BasicInvestment> getAllBasicInvestment(Customer customer) {

    return customerResource.getAllBasicInvestment(customer);
  }

  @Override
  public List<GoalCentricInvestment> getAllGoalCentricInvestment(Customer customer) {
    
    return customerResource.getAllGoalCentricInvestment(customer);
  }

}