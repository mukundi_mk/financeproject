package com.psybergate.finance.service.impl;


import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.resource.InvestmentResource;
import com.psybergate.finance.service.EventService;
import com.psybergate.finance.service.InvestmentService;
import com.psybergate.finance.service.TransactionService;
import com.sun.istack.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvestmentServiceImpl
    implements InvestmentService {
  
  static Logger logger =  Logger.getLogger(InvestmentServiceImpl.class);
  
  private InvestmentResource investmentResource;
  private TransactionService transactionService;
  private EventService eventService;

  public InvestmentServiceImpl() {
  }

  @Autowired
  public InvestmentServiceImpl(InvestmentResource investmentResource, TransactionService transactionService,  EventService eventService) {
    this.investmentResource = investmentResource;
    this.transactionService = transactionService;
    this.eventService = eventService;
  }

  @Override
  public void save(BasicInvestment investment) {
    investmentResource.persist(investment);
    generateForecastDetails(investment);
  }

  @Override
  public BasicInvestment get(long id) {
    BasicInvestment investment = investmentResource.get(id);
    investment.getEvents().size();
    investment.getTransactions().size();
    return investment;
  }

  @Override
  public void generateForecastDetails(BasicInvestment investment) {
    investment.initialiseInvestment();
  }

  @Override
  public void updateInvestment(BasicInvestment investment) {
    investmentResource.update(investment);
    transactionService.update(investment.getTransactions());
    eventService.update(investment.getEvents());
    generateForecastDetails(investment);
  }

  @Override
  public BasicInvestment get(String forecastName) {
    
    return investmentResource.get(forecastName);
  }

  @Override
  public void remove(BasicInvestment investment) {
    investmentResource.remove(investment);
  }

}
