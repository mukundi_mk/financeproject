package com.psybergate.finance.service;

import com.psybergate.finance.domain.investment.GoalCentricInvestment;

public interface GoalCentricInvestmentService {

  public abstract void save(GoalCentricInvestment investment);

  public abstract GoalCentricInvestment get(long id);
  
  public abstract GoalCentricInvestment get(String forecastName);

  public void updateInvestment(GoalCentricInvestment investment);

  public void generateForecastDetails(GoalCentricInvestment investment);
  
  public void remove(GoalCentricInvestment investment);
}
