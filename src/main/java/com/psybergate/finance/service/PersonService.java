package com.psybergate.finance.service;


import com.psybergate.finance.dao.PersonDao;
import com.psybergate.finance.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class PersonService {

    private final PersonDao personDao;

    @Autowired
    public PersonService(PersonDao personDao) {

        this.personDao = personDao;
    }


    public void addPerson(Person person){

        personDao.save(person);

    }

    public List<Person> getAllPeople(){

        List<Person> people = new ArrayList<>();
         personDao.findAll().forEach(people::add);
         return people;
    }


    public Optional<Person> selectPersonById(UUID id){

        return personDao.findById(id);
    }


    public int deletePersonById(UUID id) {
        personDao.deleteById(id);
        return 1;
    }

    public int updatePersonById(UUID id) {


        return 1;
    }
}
