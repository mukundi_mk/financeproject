package com.psybergate.finance.service;

import com.psybergate.finance.domain.bond.Bond;

public interface BondService{
  
  public abstract void save(Bond Bond);

  public abstract Bond get(long id);
  
  public abstract Bond get(String forecastName);
  
  public void updateBond(Bond bond);

  
  public void generateForecastDetails(Bond bond);

  public void remove(Bond bond);
  
}
