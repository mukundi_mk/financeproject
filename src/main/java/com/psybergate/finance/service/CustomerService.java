package com.psybergate.finance.service;

import java.util.List;

import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.customer.Customer;
import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.GoalCentricInvestment;

public interface CustomerService {

  public void registerCustomer(Customer customer);

  public Customer getCustomer(Long customerID);

  public List<Customer> getAllCustomers();

  public void remove(Customer customer);

  public List<Bond> getAllBonds(Customer customer);

  public List<BasicInvestment> getAllBasicInvestment(Customer customer);
  
  public List<GoalCentricInvestment> getAllGoalCentricInvestment(Customer customer);

}
