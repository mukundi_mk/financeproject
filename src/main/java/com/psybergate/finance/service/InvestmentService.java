package com.psybergate.finance.service;

import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.Investment;

public interface InvestmentService {

  public abstract void save(BasicInvestment investment);

  public abstract BasicInvestment get(long id);
  
  public abstract BasicInvestment get(String forecastName);

  public void updateInvestment(BasicInvestment investment);

  public void generateForecastDetails(BasicInvestment investment);
  
  public void remove(BasicInvestment investment);

}
