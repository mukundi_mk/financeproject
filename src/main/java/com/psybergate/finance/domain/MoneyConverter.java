package com.psybergate.finance.domain;

import java.math.BigDecimal;

import javax.persistence.AttributeConverter;

public class MoneyConverter implements AttributeConverter<Money, BigDecimal> {

  @Override
  public BigDecimal convertToDatabaseColumn(Money attribute) {
    BigDecimal dbValue = null;
    if (attribute == null) {
      return dbValue;
    }
    if (attribute.getAmount() != null) {
      dbValue = attribute.getAmount();
    }
    return dbValue;
  }

  @Override
  public Money convertToEntityAttribute(BigDecimal dbData) {
    Money attributeValue = null;
    if (dbData == null) {
      return attributeValue;
    }
    if (dbData != null) {
      attributeValue = new Money(dbData);
    }
    return attributeValue;
  }

}
