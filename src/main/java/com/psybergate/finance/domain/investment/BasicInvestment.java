package com.psybergate.finance.domain.investment;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;

import com.psybergate.finance.domain.Money;
import com.psybergate.finance.exceptions.InvalidAmountException;
import com.psybergate.finance.exceptions.InvalidTermException;
import com.psybergate.finance.exceptions.MonthlyDepositException;


@Entity
@NamedQuery(name = "Investment.findByForecastName", query = "SELECT i FROM BasicInvestment i where i.forecastName = :forecastName")
public class BasicInvestment extends Investment{

  private static final long serialVersionUID = 1L;

  private static final BigDecimal MIN_LUMSUM = new BigDecimal(5000);

  private static final BigDecimal MIN_MONTHLY_INVESTMENT = new BigDecimal(300);

  private static final int MIN_TERM = 6;

  private static final BigDecimal MIN_MONTHLYLUMPSUM = new BigDecimal(2000);

  private static final int MAX_TERM = 480;
  
  
  @Override
  public void initialiseInvestment() {
    generateForecastMonths();
    generateTotalInterest();
    generateFutureValue();
    generateTotalContribution();
  }
  
  private void generateFutureValue() {
    Money futureValue = getForecastMonths().get(getTerm() - 1).getClosingBalance();
    setFutureValue(futureValue);
  }
  
  private void validateLumpSum(Money initialLumpSum)
      throws InvalidAmountException {
    if (isMonthlyDepositGiven()) {

      if (isLessThanMonthlyLumSumMin(initialLumpSum)
          && initialLumpSum.getAmount().compareTo(new BigDecimal(0)) != 0)
        throw new InvalidAmountException("Initial lump sum must be 2000 or more or zero........");

    }
    else {

      if (isLumpSumlessThanMin(initialLumpSum)) {
        throw new InvalidAmountException("Initial lump sum must be 5000 or more");
      }
    }
  }

  private boolean isLessThanMonthlyLumSumMin(Money initialLumpSum) {
    return initialLumpSum.getAmount().compareTo(MIN_MONTHLYLUMPSUM) < 0;
  }

  private boolean isMonthlyDepositGiven() {
    return this.getMonthlyDeposit() != null;
  }

  private boolean isLumpSumlessThanMin(Money initialLumpSum2) {
    return initialLumpSum2.getAmount().compareTo(MIN_LUMSUM) < 0;
  }

  @Override
  public void setMonthlyDeposit(Money monthlyDeposit) {
    validateLumpSum(this.getInitialLumpSum());
    validateMonthlyDeposit(monthlyDeposit);
    this.monthlyDeposit = monthlyDeposit;
  }

  private void validateMonthlyDeposit(Money monthlyDeposit)
      throws MonthlyDepositException {

    if (isMonthlyDepositLessThanMin(monthlyDeposit)
        && monthlyDeposit.getAmount().compareTo(new BigDecimal(0)) != 0) {
      throw new MonthlyDepositException("Monthly deposit should be 300 or more");
    }

  }

  private boolean isMonthlyDepositLessThanMin(Money monthlyDeposit2) {

    return monthlyDeposit2.getAmount().compareTo(MIN_MONTHLY_INVESTMENT) < 0;
  }

  @Override
  public void setTerm(int term) {
    validateTerm(term);
    super.setTerm(term);
  }

  private void validateTerm(int term)
      throws InvalidTermException {
    if ((isLessThanMinTerm(term) || isGreaterThanMaxTerm(term)) || isNotMultipleOfSix(term)) {
      throw new InvalidTermException(
          "Term must be greater than 6 month or less than 480 inclusive and be a multiple of 6");
    }

  }

  private boolean isGreaterThanMaxTerm(int term) {
    return term > MAX_TERM;
  }

  private boolean isNotMultipleOfSix(int term) {
    return (term % MIN_TERM) != 0;
  }

  private boolean isLessThanMinTerm(int term) {
    return term < MIN_TERM;
  }

}
