package com.psybergate.finance.domain.investment;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;

import com.psybergate.finance.domain.ForecastUtil;
import com.psybergate.finance.domain.Money;

@Entity
@NamedQuery(name = "GoalCentricInvestment.findByForecastName", query = "SELECT i FROM GoalCentricInvestment i where i.forecastName = :forecastName")
public class GoalCentricInvestment extends Investment {

  private static final long serialVersionUID = 1L;
  
  private InvestmentGoal investmentGoal = InvestmentGoal.CALCULATE_MONTHLY_INSTALLMENT;

  public static enum InvestmentGoal{
    CALCULATE_INITIAL_LUMPSUM("Initial Lumpsum"), CALCULATE_MONTHLY_INSTALLMENT("Monthly Installment");

    private String name;

    private InvestmentGoal(String name) {
      this.name = name;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

  }

  public GoalCentricInvestment() {
  }

  @Override
  public void initialiseInvestment() {
    calculateBasedOnGoal();
    generateForecastMonths();
    generateTotalInterest();
    generateTotalContribution();

  }

  public InvestmentGoal[] getInvestmentGoals() {
    return InvestmentGoal.values();
  }
  private void calculateBasedOnGoal() {
    if(this.getInvestmentGoal().equals(InvestmentGoal.CALCULATE_INITIAL_LUMPSUM)) {
      calculateInitialLumpSum();
    }
    else {
      calculateMonthlyInstallment();
    }
  }

  private void calculateMonthlyInstallment() {
    Money compoundInterest = ForecastUtil.calculateCompoundInterest(this.getInterest(), this.getTerm());
    Money monthlyInterestRate = ForecastUtil.calculateMonthlyInterestRate(this.getInterest());
    
    Money monthlyInstallment = monthlyInstallmentFormula(compoundInterest, monthlyInterestRate);
    this.setMonthlyDeposit(monthlyInstallment);
  }

  private Money monthlyInstallmentFormula(Money compoundInterest, Money monthlyInterestRate) {
    System.out.println(this.getFutureValue());
    System.out.println(this.getInitialLumpSum());
    System.out.println(compoundInterest);
    System.out.println(monthlyInterestRate);
    Money numerator = (this.getFutureValue().subtract(this.getInitialLumpSum().multiply(compoundInterest))).multiply(monthlyInterestRate);
    Money demominator = compoundInterest.subtract(Money.$(1));
    
    Money monthlyInstallment =  numerator.divide(demominator);
    return monthlyInstallment;
  }

  private void calculateInitialLumpSum() {
    
    Money compoundInterest = ForecastUtil.calculateCompoundInterest(this.getInterest(), this.getTerm());
    Money monthlyInterestRate = ForecastUtil.calculateMonthlyInterestRate(this.getInterest());
    
    Money intialLumpSum = presentValueFormula(compoundInterest, monthlyInterestRate);
    
    this.setInitialLumpSum(intialLumpSum);
  }

  private Money presentValueFormula(Money compoundInterest, Money monthlyInterestRate) {
    Money firstTerm = (this.getFutureValue().divide(compoundInterest));
    Money secondTerm  =  (this.getMonthlyDeposit().multiply(compoundInterest.subtract(Money.$(1)))).divide(monthlyInterestRate.multiply(compoundInterest));
    
    Money intialLumpSum = firstTerm.subtract(secondTerm);
    return intialLumpSum;
  }

  @Override
  public void setInitialLumpSum(Money initialLumpSum) {
    this.initialLumpSum = initialLumpSum;
  }

  @Override
  public void setMonthlyDeposit(Money monthlyDeposit) {
    this.monthlyDeposit = monthlyDeposit;
  }

  public InvestmentGoal getInvestmentGoal() {
    return investmentGoal;
  }

  public void setInvestmentGoal(InvestmentGoal investmentGoal) {
    this.investmentGoal = investmentGoal;
  }
  
  

}
