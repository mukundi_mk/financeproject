package com.psybergate.finance.domain.investment;

import java.util.List;

import com.psybergate.finance.domain.Forecast;
import com.psybergate.finance.domain.ForecastMonth;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.Transaction;
import com.psybergate.finance.domain.Transaction.TransactionType;

public class InvestmentForecastMonth extends ForecastMonth{

  public InvestmentForecastMonth(int month, Money interestRate, Money openingBalance, Money monthlyDeposit) {
    super(month,interestRate,openingBalance,monthlyDeposit);
  }
  public InvestmentForecastMonth(int month, Money openingBalance) {
    super(month,openingBalance);
  }
  
  @Override
  public void generateClosingBalance(Forecast forecast) {
    Money closingBalance =  this.getOpeningBalance().add(this.getInterestForMonth()).add(this.getMonthlyDeposit());
    this.setClosingBalance(this.getClosingBalance().add(closingBalance));
  }
  
  @Override
  protected Money calcClosingbalanceWithTransaction( Forecast forecast) {
    List<Transaction> transactions = forecast.getTransactions();
    Money closingBalance = Money.ZERO;
    for (Transaction transcation : transactions) {
      if (transcation.getMonth() == this.getMonth()) {
        if (transcation.getTransactionType() == TransactionType.WITHDRAWAL) {
          setTransactionWithdrawal(transcation.getTransactionAmount());
          closingBalance = closingBalance.subtract(transcation.getTransactionAmount());
        } else {
          setTransactionDeposit(transcation.getTransactionAmount());
          closingBalance = closingBalance.add(transcation.getTransactionAmount());
        }
      }
    }
    return closingBalance;
  }
}
