package com.psybergate.finance.domain.investment;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.psybergate.finance.domain.Forecast;
import com.psybergate.finance.domain.ForecastMonth;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.MoneyConverter;
import com.psybergate.finance.domain.Transaction.TransactionType;
import com.psybergate.finance.exceptions.InvalidAmountException;
import com.psybergate.finance.exceptions.InvalidTermException;
import com.psybergate.finance.exceptions.MonthlyDepositException;

@Entity
@Table(name="investment")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class Investment extends Forecast
    implements Serializable {

  private static final long serialVersionUID = 1L;

  @Convert(converter = MoneyConverter.class)
 protected Money initialLumpSum;

  @Convert(converter = MoneyConverter.class)
 protected Money monthlyDeposit;

  @Convert(converter = MoneyConverter.class)
  private Money futureValue;

  public abstract void initialiseInvestment();

  protected void generateForecastMonths() {

    List<ForecastMonth> forecastMonths = new ArrayList<>();

    Money monthOpeningBalance = getInitialLumpSum();

    for (int i = 1; i <= this.getTerm(); i++) {
      ForecastMonth currMonthForecast = new InvestmentForecastMonth(i, getInterest(),
          monthOpeningBalance, getMonthlyDeposit());
      currMonthForecast.generateAdditionalForecastDetails(this);
      forecastMonths.add(currMonthForecast);
      monthOpeningBalance = currMonthForecast.getClosingBalance();
    }

    this.setForecastMonths(forecastMonths);
  }

  protected void generateTotalContribution() {
    setTotalContribution(new Money(getInitialLumpSum().getAmount()));
    for (ForecastMonth forecastMonth : getForecastMonths()) {
      setTotalContribution(getTotalContribution().add(forecastMonth.getMonthlyDeposit())
          .add(forecastMonth.getTransactionDeposit()));
    }
  }

  public Money getFutureValue() {
    return futureValue;
  }

  public void setFutureValue(Money futureValue) {
    this.futureValue = futureValue;
  }

  public Investment() {
  }

  public Money getInitialLumpSum() {
    return this.initialLumpSum;
  }
  public Money getMonthlyDeposit() {
    return this.monthlyDeposit;
  }

  public void setInitialLumpSum(Money initialLumpSum) {
    this.initialLumpSum = initialLumpSum;
  }

  public void setMonthlyDeposit(Money monthlyDeposit) {
  }


}
