package com.psybergate.finance.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.investment.Investment;

@Entity
public class Transaction extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  public static enum TransactionType {
    WITHDRAWAL("Withdrawal"), DEPOSIT("Deposit");

    private String name;

    public String getName() {
      return name;
    }

    private TransactionType(String name) {
      this.name = name;
    }
  }

  @ManyToOne(cascade = CascadeType.ALL)
  private Forecast forecast; 
  
  private TransactionType transactionType;
  
  @Convert(converter = MoneyConverter.class)
  private Money transactionAmount;
  
  private int month;
  
  @Transient
  private List<TransactionType> transactionTypes;

  
  public void setTransactionTypes(List<TransactionType> transactionTypes) {
    this.transactionTypes = transactionTypes;
  }

  public List<TransactionType> getTransactionTypes() {
    List<TransactionType> transactionTypes = new ArrayList<>();
    Collections.addAll(transactionTypes, Transaction.TransactionType.values());
    return transactionTypes;
  }

  public Transaction(Forecast forecast, TransactionType transactionType, Money transactionAmount, int month) {
    this.forecast= forecast;
    this.transactionType = transactionType;
    this.transactionAmount = transactionAmount;
    this.month = month;
  }

  public Transaction() {
  }
  
  public Forecast getForecast() {
    return forecast;
  }

  public void setForecast(Forecast forecast) {
    this.forecast = forecast;
  }

  public TransactionType getTransactionType() {
    return transactionType;
  }

  public void setTransactionType(TransactionType transactionType) {
    this.transactionType = transactionType;
  }

  public Money getTransactionAmount() {
    return transactionAmount;
  }

  public void setTransactionAmount(Money transactionAmount) {
    this.transactionAmount = transactionAmount;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + (int) (this.getId() ^ (this.getId() >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (!super.equals(obj))
      return false;
    if (getClass() != obj.getClass())
      return false;
    Transaction other = (Transaction) obj;
    if (this.getId() != other.getId())
      return false;
    return true;
  }

  

}
