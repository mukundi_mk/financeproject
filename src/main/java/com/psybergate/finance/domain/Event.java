package com.psybergate.finance.domain;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.psybergate.finance.domain.bond.Bond;

@Entity
public class Event extends BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @ManyToOne(cascade = CascadeType.ALL)
  private Forecast forecast;

  private int startMonth;

  private int endMonth;

  @Convert(converter = MoneyConverter.class)
  private Money amount;

  private EventType eventType;

  public static enum EventType {
    INTEREST_CHANGE("Interest Change"), AMOUNT_CHANGE("Amount Change"), AMOUNT_INCREASE("Amount Increase");

    private String name;

    public String getName() {
      return name;
    }

    private EventType(String name) {
      this.name = name;
    }
  }

  public EventType[] getEventTypes() {
    return EventType.values();
  }

  public Event(EventType eventType, int startMonth, int endMonth, Money amount) {
    this.startMonth = startMonth;
    this.endMonth = endMonth;
    this.amount = amount;
    this.eventType = eventType;
  }

  public Event() {
  }

  public Event(Forecast forecast, EventType eventType, int startMonth, int endMonth, Money amount) {
    this.forecast = forecast;
    this.eventType = eventType;
    this.startMonth =  startMonth;
    this.endMonth = endMonth;
    this.amount = amount;
  }

  public Forecast getForecast() {
    return forecast;
  }

  public void setForecast(Forecast forecast) {
    this.forecast = forecast;
  }

  public int getStartMonth() {
    return startMonth;
  }

  public void setStartMonth(int startMonth) {
    this.startMonth = startMonth;
  }

  public int getEndMonth() {
    return endMonth;
  }

  public void setEndMonth(int endMonth) {
    this.endMonth = endMonth;
  }

  public Money getAmount() {
    return amount;
  }

  public void setAmount(Money amount) {
    this.amount = amount;
  }

  public EventType getEventType() {
    return eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }
//
//  @Override
//  public int hashCode() {
//    final int prime = 31;
//    int result = super.hashCode();
//    result = prime * result + (int) (this.getId() ^ (this.getId() >>> 32));
//    return result;
//  }
//
//  @Override
//  public boolean equals(Object obj) {
//    if (this == obj)
//      return true;
//    if (!super.equals(obj))
//      return false;
//    if (getClass() != obj.getClass())
//      return false;
//    Event other = (Event) obj;
//    if (this.getId() != other.getId())
//      return false;
//    return true;
//  }

}
