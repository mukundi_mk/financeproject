package com.psybergate.finance.domain;

public class ForecastUtil {
  
  private static final double INTEREST_PERIOD = 12.0;

  private static final double PERCENTAGE_DENOMINATOR = 100.0;
  
  public static Money calculateMonthlyInterestRate(Money interestRate) {
    Money monthlyInterestRate = interestRate.divide(PERCENTAGE_DENOMINATOR)
        .divide(INTEREST_PERIOD);
    return monthlyInterestRate;
  }

  public static Money monthlyInstallmentFormula(Money compoundInterest, Money principalAmount,
      Money monthlyRate, Money balloonPayment) {
    return ((principalAmount.multiply(compoundInterest).subtract(balloonPayment))
        .multiply(monthlyRate)).divide(compoundInterest.subtract(new Money(1)));
  }
  

  public static Money calculateCompoundInterest(Money interest, int term ) {
    Money sInterest = ForecastUtil.calculateMonthlyInterestRate(interest).add(new Money(1));
    Money compound = new Money(sInterest.getAmountForCalc().pow(term));
    return compound;
  }
}
