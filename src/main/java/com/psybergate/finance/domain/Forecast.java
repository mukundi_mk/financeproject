package com.psybergate.finance.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.psybergate.finance.domain.customer.Customer;

@Entity
@Table(name="forecast")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Forecast extends BaseEntity {

  private static final int DEFAULT_TERM = 60;

  @Column(unique = true)
  private String forecastName;

  @Convert(converter = MoneyConverter.class)
  private Money interest;

  private int term = DEFAULT_TERM;

  @ManyToOne
  @JoinColumn(name = "customer_id")
  private Customer customer;

  @Transient
  private Money totalInterest;

  @Transient
  private List<ForecastMonth> forecastMonths;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Transaction> transactions = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Event> events = new ArrayList<>();
  
  @Transient
  private Money totalContribution;

  public void generateTotalInterest() {
    Money totalInterest = Money.ZERO;
    List<ForecastMonth> forecastMonths = this.getForecastMonths();
    for (ForecastMonth forecastMonth : forecastMonths) {
      totalInterest = totalInterest.add(forecastMonth.getInterestForMonth());
    }
    setTotalInterest(totalInterest);
  }
  
  
  //change to protected
  public ForecastMonth getLastForecastMonth() {
    return this.getForecastMonths().get(forecastMonths.size()-1);
  }

  
  
  public Money getTotalContribution() {
    return totalContribution;
  }

  
  public void setTotalContribution(Money totalContribution) {
    this.totalContribution = totalContribution;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public List<ForecastMonth> getForecastMonths() {
    return forecastMonths;
  }

  public void setForecastMonths(List<ForecastMonth> forecastRows) {
    this.forecastMonths = forecastRows;
  }

  public List<Transaction> getTransactions() {
    return transactions;
  }

  public void setTransactions(List<Transaction> transactions) {
    this.transactions = transactions;
  }

  public List<Event> getEvents() {
    return events;
  }

  public void setEvents(List<Event> events) {
    this.events = events;
  }

  public String getForecastName() {
    return forecastName;
  }

  public void setForecastName(String forecastName) {
    this.forecastName = forecastName;
  }

  public Money getInterest() {
    return interest;
  }

  public void setInterest(double interest) {
    this.interest = new Money(interest);
  }

  public void setInterest(Money interest) {
    this.interest = interest;
  }

  public int getTerm() {
    return term;
  }

  public void setTerm(int term) {
    this.term = term;
  }

  public Money getTotalInterest() {
    return totalInterest;
  }

  public void setTotalInterest(Money totalInterest) {
    this.totalInterest = totalInterest;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((forecastName == null) ? 0 : forecastName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Forecast other = (Forecast) obj;
    if (forecastName == null) {
      if (other.forecastName != null)
        return false;
    }
    else if (!forecastName.equals(other.forecastName))
      return false;
    return true;
  }

}
