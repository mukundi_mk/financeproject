package com.psybergate.finance.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Money {

  private BigDecimal amount;

  public static final Money ZERO = new Money(0);

  public Money() {
  }

  public static Money $(double amount) {
    return new Money(amount);
  }
  
  public static Money $(BigDecimal amount) {
    return new Money(amount);
  }

  public BigDecimal getAmount() {
    return this.amount.setScale(2, RoundingMode.DOWN);
  }
  
  public BigDecimal getAmountForCalc() {
    return this.amount;
  }

  public Money(BigDecimal amount) {
    this.amount = amount;
  }

  public Money(double amount) {
    this.amount = new BigDecimal(amount);
  }

  public Money add(Money money) {
    return new Money(this.amount.add(money.amount));
  }

  public Money subtract(Money money) {
    return new Money(this.amount.subtract(money.amount));
  }

  public Money multiply(BigDecimal rate) {
    return new Money(this.amount.multiply(rate));
  }

  public Money multiply(Money money) {
    return new Money(this.amount.multiply(money.getAmountForCalc()));
  }

  public Money divide(Money money) {
    return new Money(this.amount.divide(money.getAmountForCalc(),9,RoundingMode.DOWN));
  }

  public Money divide(BigDecimal rate) {
    return new Money(this.amount.divide(rate, 9, RoundingMode.DOWN));
  }
  
  public Money divide(double rate) {
    return new Money(this.amount.divide(new BigDecimal(rate), 9, RoundingMode.DOWN));
  }

  public int compareTo(Money money) {
    return this.getAmount().compareTo(money.getAmountForCalc());
  }


  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Money other = (Money) obj;
    if (amount == null) {
      if (other.amount != null)
        return false;
    } else if (!amount.equals(other.amount))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return String.valueOf(amount.doubleValue());
  }

  
  
}
