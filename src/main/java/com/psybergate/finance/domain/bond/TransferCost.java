package com.psybergate.finance.domain.bond;

import java.util.ArrayList;
import java.util.List;

import com.psybergate.finance.domain.Money;

public class TransferCost {

  private List<TaxBracket> brackets;

  private Money transferCost;
  
  public TransferCost() {
    initializeBrackets();
    setTransferCost(Money.ZERO);
  }

  public Money calculateTranferCost(Money initialLoan) {
    Money amount = Money.ZERO, taxableAmount = Money.ZERO, taxPayable = Money.ZERO;
    amount = initialLoan;
    for (int index = 0; index < brackets.size(); index++) {
      TaxBracket currBracket = brackets.get(index);
      if (amount.compareTo(currBracket.getHigh().subtract(currBracket.getLow())) <= 0) {
        taxableAmount = amount;
      }
      else {
        taxableAmount = currBracket.getHigh().subtract(currBracket.getLow());
      }
      amount = amount.subtract(taxableAmount);
      taxPayable = taxableAmount.multiply(currBracket.getPercentage());
      setTransferCost(getTransferCost().add(taxPayable));
    }
    return getTransferCost();
  }
  
  private void initializeBrackets() {
    brackets = new ArrayList<>();
    brackets.add(TaxBracket.FIRST);
    brackets.add(TaxBracket.SECOND);
    brackets.add(TaxBracket.THIRD);
    brackets.add(TaxBracket.FORTH);
    brackets.add(TaxBracket.FIFTH);
    brackets.add(TaxBracket.SIXTH);
  }
  
  public Money getTransferCost() {
    return transferCost;
  }

  public void setTransferCost(Money transferCost) {
    this.transferCost = transferCost;
  }

}
