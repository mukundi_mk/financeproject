package com.psybergate.finance.domain.bond;

import java.math.BigDecimal;

import com.psybergate.finance.domain.Money;

public class BondLegalCost {

  private Money convayancersFee;

  private Money placeHolder;

  private Money remainigBal;

  public enum PricingGuidelines {

    FIRSTBRACKET(MoneyConstants.LegalCost.FIRST_lOWER_BOUND, MoneyConstants.LegalCost.FIRST_UPPER_BOUND,
        MoneyConstants.LegalCost.FIRST_COST, MoneyConstants.LegalCost.FIRST_LIMIT),
    
    SECONDBRACKET(MoneyConstants.LegalCost.FIRST_UPPER_BOUND, MoneyConstants.LegalCost.SECOND_UPPER_BOUND,
        MoneyConstants.LegalCost.SECOND_COST, MoneyConstants.LegalCost.SECOND_LIMIT),
    
    THIRDBRACKET(MoneyConstants.LegalCost.FIRST_lOWER_BOUND, MoneyConstants.LegalCost.THIRD_UPPER_BOUND,
        MoneyConstants.LegalCost.SECOND_COST, MoneyConstants.LegalCost.THIRD_LIMIT),
    
    FOURTHBRACKET(MoneyConstants.LegalCost.THIRD_UPPER_BOUND, MoneyConstants.LegalCost.FORTH_UPPER_BOUND,
        MoneyConstants.LegalCost.FORTH_COST, MoneyConstants.LegalCost.FORTH_LIMIT);

    Money lowerBound;

    Money upperBound;

    Money cost;

    Money limitPrice;

    public Money getLowerBound() {
      return lowerBound;
    }

    public Money getUpperBound() {
      return upperBound;
    }

    public Money getCost() {
      return cost;
    }

    public Money getLimitPrice() {
      return limitPrice;
    }

    private PricingGuidelines(Money lowerBound, Money upperBound, Money cost, Money limitPrice) {
      this.lowerBound = lowerBound;
      this.upperBound = upperBound;
      this.cost = cost;
      this.limitPrice = limitPrice;
    }
  }

  public Money calculateLegalCost(Money initialLoan) {
    final Money convayancersFee = new BondLegalCost().generateConvayencersCost(initialLoan);
    final Money deedsRegistryFee = MoneyConstants.LegalCost.DEEDS_REGISTRY_FEE;

    Money legalCost = deedsRegistryFee.add(convayancersFee);
    return legalCost;

  }

  public Money getConvayancersFee() {
    return convayancersFee;
  }

  public void setConvayancersFee(Money convayancersFees) {
    convayancersFee = convayancersFees;
  }

  public Money generateConvayencersCost(Money bondValue) {
    placeHolder = new Money(MoneyConstants.LegalCost.FIRST_lOWER_BOUND.getAmount());
    remainigBal = bondValue.subtract(MoneyConstants.LegalCost.FIRST_lOWER_BOUND);
    setConvayancersFee(new Money(MoneyConstants.LegalCost.BASE_COST.getAmount()));
    PricingGuidelines[] pricingGuidelines = PricingGuidelines.values();
    for (int index = 0; index < pricingGuidelines.length; index++) {
      calcConvayencersCost(pricingGuidelines[index]);
      
    }
    return getConvayancersFee();
  }

  private void calcConvayencersCost(PricingGuidelines bracket) {

    while (remainigBal.compareTo(bracket.getLimitPrice()) > 0
        && placeHolder.compareTo(bracket.getUpperBound()) <= 0) {
      if (placeHolder.compareTo(bracket.getLowerBound()) >= 0) {
        setConvayancersFee(getConvayancersFee().add(bracket.getCost()));
        placeHolder = placeHolder.add(bracket.getLimitPrice());
        remainigBal = remainigBal.subtract(bracket.getLimitPrice());
      }

    }
    if (remainigBal.compareTo(Money.ZERO) > 0
        && remainigBal.compareTo(bracket.getLimitPrice()) <= 0) {
      setConvayancersFee(getConvayancersFee().add(bracket.getCost()));
      remainigBal = Money.ZERO;
    }
  }

}
