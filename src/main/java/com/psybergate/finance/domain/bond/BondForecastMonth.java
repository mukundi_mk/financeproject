package com.psybergate.finance.domain.bond;

import java.util.List;

import com.psybergate.finance.domain.Forecast;
import com.psybergate.finance.domain.ForecastMonth;
import com.psybergate.finance.domain.ForecastUtil;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.Transaction;
import com.psybergate.finance.domain.Transaction.TransactionType;
import com.psybergate.finance.domain.bond.Bond.UpdateGoal;

public class BondForecastMonth extends ForecastMonth {

  public BondForecastMonth(int month, Money openingBalance, Money monthlyRepayment) {
    super(month, openingBalance, monthlyRepayment);
  }

  public BondForecastMonth(int month, Money interestRate, Money openingBalance,
      Money monthlyDeposit) {
    super(month, interestRate, openingBalance, monthlyDeposit);
  }

  @Override
  public void generateClosingBalance(Forecast forecast) {
    Bond bond = (Bond) forecast;
    Money closingBalance = this.getOpeningBalance().add(this.getInterestForMonth())
        .subtract(this.getMonthlyDeposit());

    this.setClosingBalance(this.getClosingBalance().add(closingBalance));
    if (bond.getGoal().equals(UpdateGoal.REDUCE_TERM)) {
      if (this.getClosingBalance().compareTo(bond.getMonthlyRepayment()) < 0) {
        Money repayment = bond.getMonthlyRepayment()
            .add(this.getClosingBalance().subtract(bond.getBalloonPayment()));
        this.setMonthlyDeposit(repayment);
        closingBalance = bond.getBalloonPayment();
        this.setClosingBalance(closingBalance);
      }
    }
    else if (bond.getGoal().equals(UpdateGoal.REDUCE_REPAYMENT)) {
      if (this.isHasEvent() || this.isHasTransaction()) {
        reCalculateRecommendedMonthlyRepayment(bond);
      }
    }


  }

  private void reCalculateRecommendedMonthlyRepayment(Bond bond) {
    int newTerm = bond.getTerm() - this.getMonth();
    Money compoundInterest = ForecastUtil.calculateCompoundInterest(bond.getInterest(), newTerm);
    Money monthlyRate = ForecastUtil.calculateMonthlyInterestRate(bond.getInterest());
    Money balloonPayment = bond.getBalloonPayment();
    Money recommendedMonthlyRepayment = ForecastUtil.monthlyInstallmentFormula(compoundInterest, this.getClosingBalance(),
        monthlyRate, balloonPayment);
    bond.setRecommendedMonthlyRepayment(recommendedMonthlyRepayment);
  }

  @Override
  protected Money calcClosingbalanceWithTransaction(Forecast forecast) {
    List<Transaction> transactions = forecast.getTransactions();
    Money closingBalance = Money.ZERO;
    for (Transaction transcation : transactions) {
      if (transcation.getMonth() == this.getMonth()) {
        setHasTransaction(true);
        if (transcation.getTransactionType() == TransactionType.WITHDRAWAL) {
          setTransactionWithdrawal(transcation.getTransactionAmount());
          closingBalance = closingBalance.add(transcation.getTransactionAmount());
        }
        else {
          setTransactionDeposit(transcation.getTransactionAmount());
          closingBalance = closingBalance.subtract(transcation.getTransactionAmount());
        }
      }
    }
    return closingBalance;
  }

}
