package com.psybergate.finance.domain.bond;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import com.psybergate.finance.domain.Forecast;
import com.psybergate.finance.domain.ForecastMonth;
import com.psybergate.finance.domain.ForecastUtil;
import com.psybergate.finance.domain.Money;
import com.psybergate.finance.domain.MoneyConverter;
import com.psybergate.finance.exceptions.InvalidTermException;
import com.psybergate.finance.web.controller.util.RequestUtil;

@Entity
@NamedQuery(name = "Bond.findByForecastName", query = "SELECT i FROM Bond i where i.forecastName = :forecastName")
public class Bond extends Forecast
    implements Serializable {

  private static final long serialVersionUID = 1L;

  private static final int MIN_TERM = 6;

  private static final int MAX_TERM = 360;

  private static final Money BANK_INITIATION_FEE = new Money(6037);

  public static enum UpdateGoal {
    REDUCE_TERM, REDUCE_REPAYMENT
  }

  @Column(nullable = false)
  @Convert(converter = MoneyConverter.class)
  private Money initialLoan;

  @Column(nullable = false)
  @Convert(converter = MoneyConverter.class)
  private Money monthlyRepayment = Money.ZERO;

  @Column(nullable = false)
  @Convert(converter = MoneyConverter.class)
  private Money deposit;

  @Column(nullable = false)
  @Convert(converter = MoneyConverter.class)
  private Money balloonPayment = Money.ZERO;

  @Transient
  private Money recommendedMonthlyRepayment;

  @Transient
  private Money transferCost;

  @Transient
  private Money bondCost;

  @Transient
  private Money legalCost;

  @Transient
  private Money principalAmount;

  @Transient
  private Money totalFirstPayment;
  
  @Transient
  private Money totalFirstPaymentWAC;

  @Transient
  private int reducedTerm;

  
  public Money getTotalFirstPaymentWAC() {
    return totalFirstPaymentWAC;
  }

  
  public void setTotalFirstPaymentWAC(Money totalFirstPaymentWAC) {
    this.totalFirstPaymentWAC = totalFirstPaymentWAC;
  }

  private UpdateGoal goal = UpdateGoal.REDUCE_REPAYMENT;

  public Bond() {
  }
  
  public UpdateGoal[] getUpdateGoals() {
    return UpdateGoal.values();
  }

  public void initialiseBond() {
    calculateAdditionalCosts();
    calculateRecommendedMonthlyRepayment();
    generateForecastMonths();
    generateTotalInterest();
    calculateTotalFirstPayment();
    generateTotalContribution();
  }

  private void calculateAdditionalCosts() {
    calculateTransferCosts();
    calculateLegalCost();
    initialiseBondCosts();
  }

  private void calculateLegalCost() {
    BondLegalCost bondLegalCost = new BondLegalCost();
    Money legalCost = bondLegalCost.calculateLegalCost(getInitialLoan());
    setLegalCost(legalCost);
  }

  private void initialiseBondCosts() {
    setBondCost(BANK_INITIATION_FEE);
  }

  private void calculateTransferCosts() {
    TransferCost taxBracket = new TransferCost();
    Money transferCost = taxBracket.calculateTranferCost(getInitialLoan());
    setTransferCost(transferCost);
  }

  private void calculateTotalFirstPayment() {
    setTotalFirstPayment(
        getTransferCost().add(getDeposit()).add(getMonthlyRepayment()).add(getLegalCost()).add(getBondCost()));
    setTotalFirstPaymentWAC(getDeposit());
  }

  private void generateForecastMonths() {

    List<ForecastMonth> forecastMonths = new ArrayList<>();

    Money monthOpeningBalance = calculatePrincipalAmount();

    for (int i = 1; i <= this.getTerm(); i++) {

      ForecastMonth currMonthForecast = new BondForecastMonth(i, getInterest(),
          monthOpeningBalance, getMonthlyRepayment());
      currMonthForecast.generateAdditionalForecastDetails(this);
      forecastMonths.add(currMonthForecast);
      monthOpeningBalance = currMonthForecast.getClosingBalance();

      if (currMonthForecast.getClosingBalance().compareTo(this.getBalloonPayment()) == 0) {
        break;
      }

    }

    setReducedTerm(forecastMonths.size());

    this.setForecastMonths(forecastMonths);

  }

  private void calculateRecommendedMonthlyRepayment() {

    Money compoundInterest = ForecastUtil.calculateCompoundInterest(getInterest(), getTerm());
    Money principalAmount = calculatePrincipalAmount();
    Money monthlyRate = ForecastUtil.calculateMonthlyInterestRate(getInterest());
    Money balloonPayment = this.getBalloonPayment();

    Money recommendedMonthlyRepayment = ForecastUtil.monthlyInstallmentFormula(compoundInterest, principalAmount,
        monthlyRate, balloonPayment);
    setRecommendedMonthlyRepayment(recommendedMonthlyRepayment);
    if (getMonthlyRepayment().compareTo(getRecommendedMonthlyRepayment()) > 0) {
      setGoal(UpdateGoal.REDUCE_TERM);
    }
    
    if (monthlyRepaymentNotGiven()) {
      setMonthlyRepayment(recommendedMonthlyRepayment);
    }
  }

  private void generateTotalContribution() {
    setTotalContribution(new Money(getDeposit().getAmount()));
    for (ForecastMonth forecastMonth : getForecastMonths()) {
      setTotalContribution(getTotalContribution().add(forecastMonth.getMonthlyDeposit())
          .add(forecastMonth.getTransactionDeposit()));
    }
  }

  private boolean monthlyRepaymentNotGiven() {
    return getMonthlyRepayment().getAmount().equals(Money.ZERO.getAmount());
  }

  public UpdateGoal getGoal() {
    return goal;
  }

  public void setGoal(UpdateGoal goal) {
    this.goal = goal;
  }

  public Money getLegalCost() {
    return legalCost;
  }

  public void setLegalCost(Money legalCost) {
    this.legalCost = legalCost;
  }

  public Money getBondCost() {
    return bondCost;
  }

  public void setBondCost(Money bondCost) {
    this.bondCost = bondCost;
  }

  public Money getRecommendedMonthlyRepayment() {
    return recommendedMonthlyRepayment;
  }

  public void setRecommendedMonthlyRepayment(Money recommendedMonthlyRepayment) {
    this.recommendedMonthlyRepayment = recommendedMonthlyRepayment;
  }

  public Money getTotalFirstPayment() {
    return totalFirstPayment;
  }

  public void setTotalFirstPayment(Money totalFirstPayment) {
    this.totalFirstPayment = totalFirstPayment;
  }

  public Money getTransferCost() {
    return transferCost;
  }

  public void setTransferCost(Money transferCost) {
    this.transferCost = transferCost;
  }

  public int getReducedTerm() {
    return reducedTerm;
  }

  public void setReducedTerm(int reducedTerm) {
    this.reducedTerm = reducedTerm;
  }

  private Money calculatePrincipalAmount() {
    return this.getInitialLoan().subtract(this.getDeposit());

  }

  @Override
  public void setTerm(int term) {
    validateTerm(term);
    super.setTerm(term);
  }

  private void validateTerm(int term)
      throws InvalidTermException {
    if (isLessThanMinTerm(MIN_TERM) || isNotMultipleOfSix(term) || isGreaterThanMaxTerm(term)) {
      throw new InvalidTermException("Term " + term + "must be greater than 6 month, less than "
          + MAX_TERM + " and be a multiple of 6: ");
    }
  }

  private boolean isNotMultipleOfSix(int term) {
    return (term % 6) != 0;
  }

  private boolean isLessThanMinTerm(int term) {
    return (term < MIN_TERM);
  }

  private boolean isGreaterThanMaxTerm(int term) {
    return (term > MAX_TERM);
  }

  public Money getInitialLoan() {
    return initialLoan;
  }

  public void setInitialLoan(Money initialLoan) {
    this.initialLoan = initialLoan;
  }

  public Money getMonthlyRepayment() {
    return monthlyRepayment;
  }

  public void setMonthlyRepayment(Money monthlyRepayment) {
    this.monthlyRepayment = monthlyRepayment;
  }

  public Money getDeposit() {
    return deposit;
  }

  public void setDeposit(Money deposit) {
    this.deposit = deposit;
  }

  public Money getBalloonPayment() {
    return balloonPayment;
  }

  public void setBalloonPayment(Money balloonPayment) {
    this.balloonPayment = balloonPayment;
  }

}
