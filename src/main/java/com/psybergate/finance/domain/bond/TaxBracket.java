package com.psybergate.finance.domain.bond;

import java.math.BigDecimal;

import com.psybergate.finance.domain.Money;

public enum TaxBracket{
  
  FIRST(Money.ZERO, MoneyConstants.TaxBracket.FIRST_UPPER_LIMIT, MoneyConstants.TaxBracket.FIRST_RATE),
  
  SECOND(MoneyConstants.TaxBracket.SECOND_LOWER_LIMIT, MoneyConstants.TaxBracket.SECOND_UPPER_LIMIT,
      MoneyConstants.TaxBracket.SECOND_RATE),
  
  THIRD(MoneyConstants.TaxBracket.THIRD_LOWER_LIMIT, MoneyConstants.TaxBracket.THIRD_UPPER_LIMIT,
      MoneyConstants.TaxBracket.THIRD_RATE),
  
  FORTH(MoneyConstants.TaxBracket.FORTH_LOWER_LIMIT, MoneyConstants.TaxBracket.FORTH_UPPER_LIMIT,
      MoneyConstants.TaxBracket.FORTH_RATE),
  
  FIFTH(MoneyConstants.TaxBracket.FIFTH_LOWER_LIMIT, MoneyConstants.TaxBracket.FIFTH_UPPER_LIMIT,
      MoneyConstants.TaxBracket.FIFTH_RATE),
  
  SIXTH(MoneyConstants.TaxBracket.SIXTH_LOWER_LIMIT, MoneyConstants.TaxBracket.SIXTH_UPPER_LIMIT,
      MoneyConstants.TaxBracket.SIXTH_RATE);
  
  
  private Money low;

  private Money high;

  private BigDecimal percentage;
  
  private TaxBracket(Money low, Money high, BigDecimal percentage) {
    this.low = low;
    this.high = high;
    this.percentage = percentage;
  }

  public Money getLow() {
    return low;
  }
  public Money getHigh() {
    return high;
  }
  public BigDecimal getPercentage() {
    return percentage;
  }
  
  public void setHigh(Money high) {
    this.high = high;
  }
}
