package com.psybergate.finance.domain.bond;

import java.math.BigDecimal;

import com.psybergate.finance.domain.Money;

public interface MoneyConstants {
  interface TaxBracket{
    public static final Money FIRST_LOWER_LIMIT = Money.ZERO;
    public static final Money FIRST_UPPER_LIMIT = new Money(1_000_000);
    public static final BigDecimal FIRST_RATE = new BigDecimal(0.0);
    
    public static final Money SECOND_LOWER_LIMIT = new Money(1_000_001);
    public static final Money SECOND_UPPER_LIMIT = new Money(1_375_000);
    public static final BigDecimal SECOND_RATE = new BigDecimal(0.03);
    
    public static final Money THIRD_LOWER_LIMIT = new Money(1_375_001);
    public static final Money THIRD_UPPER_LIMIT = new Money(1_925_000);
    public static final BigDecimal THIRD_RATE = new BigDecimal(0.06);
    
    public static final Money FORTH_LOWER_LIMIT = new Money(1_925_001);
    public static final Money FORTH_UPPER_LIMIT = new Money(2_475_000);
    public static final BigDecimal FORTH_RATE = new BigDecimal(0.08);
    
    public static final Money FIFTH_LOWER_LIMIT = new Money(2_475_001);
    public static final Money FIFTH_UPPER_LIMIT = new Money(11_000_000);
    public static final BigDecimal FIFTH_RATE = new BigDecimal(0.0);
    
    public static final Money SIXTH_LOWER_LIMIT = new Money(11_000_001);
    public static final Money SIXTH_UPPER_LIMIT = new Money(1_000_000_000);
    public static final BigDecimal SIXTH_RATE = new BigDecimal(0.13);
  }
  
  interface LegalCost{
    public static final Money FIRST_lOWER_BOUND = new Money(100_000);

    public static final Money FIRST_UPPER_BOUND = new Money(500_000);

    public static final Money FIRST_COST = new Money(800);
    public static final Money FIRST_LIMIT = new Money(50000);

    public static final Money SECOND_UPPER_BOUND = new Money(1_000_000);

    public static final Money SECOND_COST = new Money(1600);
    public static final Money SECOND_LIMIT = new Money(100000);

    public static final Money THIRD_UPPER_BOUND = new Money(5_000_000);
    public static final Money THIRD_LIMIT = new Money(200000);

    public static final Money FORTH_UPPER_BOUND = new Money(5_000_000_000.00);
    public static final Money FORTH_COST = new Money(2000);
    public static final Money FORTH_LIMIT = new Money(500000);

    public static final Money BASE_COST = new Money(5200);
    
    
    public static final Money DEEDS_REGISTRY_FEE = new Money(2680.00);
  }
}