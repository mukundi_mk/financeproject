package com.psybergate.finance.domain;

import java.util.List;

import com.psybergate.finance.domain.Event.EventType;

public abstract class ForecastMonth {


  private int month;

  private Money interestRate;

  private Money monthlyDeposit;

  private Money openingBalance;

  private Money interestForMonth;

  private Money closingBalance;

  private boolean hasEvent = false;

  private boolean hasTransaction = false;

  private Money transactionDeposit;

  private Money transactionWithdrawal;
  
  public abstract void generateClosingBalance(Forecast forecast);
  
  protected abstract Money calcClosingbalanceWithTransaction(Forecast forecast);
  
  public ForecastMonth() {
  }
  
  public ForecastMonth(int month, Money interestRate, Money openingBalance, Money monthlyDeposit) {
    super();
    this.month = month;
    this.interestRate = interestRate;
    this.monthlyDeposit = monthlyDeposit;
    this.openingBalance = openingBalance;
    initializeTransaction();
    
  }
  
  public ForecastMonth(int month, Money openingBalance, Money monthlyDeposit) {
    this.month = month;
    this.monthlyDeposit = monthlyDeposit;
    this.openingBalance = openingBalance;
    initializeTransaction();
  }
  
  public ForecastMonth(int month, Money openingBalance) {
    super();
    this.month = month;
    this.openingBalance = openingBalance;
    initializeTransaction();
  }
  
  private void initializeTransaction() {
    setTransactionDeposit(new Money(0.00));
    setTransactionWithdrawal(new Money(0.00));
  }

  public void generateInterestForMonth(Forecast forecast) {
    Money monthlyInterest = this.getOpeningBalance()
        .multiply(ForecastUtil.calculateMonthlyInterestRate(this.getInterestRate()));
    this.setInterestForMonth(monthlyInterest);
  }
  
  
  public void generateAdditionalForecastDetails(Forecast forecast) {
    initialiseForecastEvents(forecast);
    generateInterestForMonth(forecast);
    this.setClosingBalance(calcClosingbalanceWithTransaction(forecast));
    generateClosingBalance(forecast);
    
  }
  
  
  public void initialiseForecastEvents(Forecast forecast) {
    List<Event> events = forecast.getEvents();
    
    for (Event event : events) {
      if (this.getMonth() >= event.getStartMonth() && this.getMonth() <= event.getEndMonth()) {
        setHasEvent(true);
        if (event.getEventType().equals(EventType.INTEREST_CHANGE)) {
          this.setInterestRate(event.getAmount());
        }
        
        if (event.getEventType().equals(EventType.AMOUNT_CHANGE)) {
          this.setMonthlyDeposit(event.getAmount());
        }
        
        if (event.getEventType().equals(EventType.AMOUNT_INCREASE)) {
          this.setMonthlyDeposit(this.getMonthlyDeposit().add(event.getAmount()));
        }
      }
    }
  }
  
  public Money getTransactionDeposit() {
    return transactionDeposit;
  }

  public void setTransactionDeposit(Money transactionDeposit) {
    this.transactionDeposit = transactionDeposit;
  }

  public Money getTransactionWithdrawal() {
    return transactionWithdrawal;
  }

  public void setTransactionWithdrawal(Money transactionWithdrawal) {
    this.transactionWithdrawal = transactionWithdrawal;
  }

  public Money getInterestRate() {
    return interestRate;
  }

  public void setInterestRate(Money interestRate) {
    this.interestRate = interestRate;
  }

  public int getMonth() {
    return month;
  }

  public void setMonth(int month) {
    this.month = month;
  }

  public Money getMonthlyDeposit() {
    return monthlyDeposit;
  }

  public void setMonthlyDeposit(Money monthlyDeposit) {
    this.monthlyDeposit = monthlyDeposit;
  }

  public Money getOpeningBalance() {
    return openingBalance;
  }

  public void setOpeningBalance(Money openingBalance) {
    this.openingBalance = openingBalance;
  }

  public Money getInterestForMonth() {
    return interestForMonth;
  }

  protected void setInterestForMonth(Money interestForMonth) {
    this.interestForMonth = interestForMonth;
  }

  public Money getClosingBalance() {
    return closingBalance;
  }

  public void setClosingBalance(Money closingBalance) {
    this.closingBalance = closingBalance;
  }

  public boolean isHasEvent() {
    return hasEvent;
  }

  public void setHasEvent(boolean hasEvent) {
    this.hasEvent = hasEvent;
  }

  public boolean isHasTransaction() {
    return hasTransaction;
  }

  public void setHasTransaction(boolean hasTransaction) {
    this.hasTransaction = hasTransaction;
  }



}
