package com.psybergate.finance.domain.customer;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.psybergate.finance.domain.BaseEntity;
import com.psybergate.finance.domain.bond.Bond;
import com.psybergate.finance.domain.investment.BasicInvestment;
import com.psybergate.finance.domain.investment.GoalCentricInvestment;
import com.psybergate.finance.domain.investment.Investment;
import com.psybergate.finance.exceptions.UnderAgeException;

@Entity
public class Customer extends BaseEntity {

  private String name ;

  @Column(unique = true)
  private String email;

  private String surname;

  private LocalDate dateOfBirth;
  
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<BasicInvestment> basicInvestments = new ArrayList<>();
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<GoalCentricInvestment> goalInvestments = new ArrayList<>();
  
  public List<BasicInvestment> getBasicInvestments() {
    return basicInvestments;
  }

  
  public void setBasicInvestments(List<BasicInvestment> basicInvestments) {
    this.basicInvestments = basicInvestments;
  }

  
  public List<GoalCentricInvestment> getGoalInvestments() {
    return goalInvestments;
  }

  
  public void setGoalInvestments(List<GoalCentricInvestment> goalInvestments) {
    this.goalInvestments = goalInvestments;
  }

  @Transient
  private List<Investment> investments = new ArrayList<>();
  
  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Bond> bonds = new ArrayList<>();
  
  @Transient
  private int age;

  public Customer() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  
  public int getAge() {
    Period period = Period.between(dateOfBirth, LocalDate.now());
    return period.getYears();
  }

  
  public void setAge(int age) {
    this.age = age;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    validateAge(dateOfBirth);
    this.dateOfBirth = dateOfBirth;
  }
  
  public List<Investment> getInvestments() {
    investments.addAll(basicInvestments);
    investments.addAll(goalInvestments);
    return investments;
  }

  
  public void setInvestments(List<Investment> investments) {
    this.investments = investments;
  }

  
  public List<Bond> getBonds() {
    return bonds;
  }

  
  public void setBonds(List<Bond> bonds) {
    this.bonds = bonds;
  }

  private void validateAge(LocalDate dateOfBirth) {
    if (isCustomerUnderAge(dateOfBirth)) {
      throw new UnderAgeException("You are under age");
    }
  }

  private Boolean isCustomerUnderAge(LocalDate dateOfBirth2) {
    LocalDate dob = dateOfBirth2;
    LocalDate now = LocalDate.now();
    Period difference = Period.between(dob, now);
    int ageOfCustomer = difference.getYears();
    int minAllowedAge = 18;

    return ageOfCustomer < minAllowedAge;
  }

}
