package com.psybergate.finance.domain;

import java.time.LocalDateTime;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE)
  private long id;

  private int version;

  private LocalDateTime timeCaptured;

  private LocalDateTime timeLastModified;

  private String userCaptured;

  private String userLastModified;

  public BaseEntity() {
    LocalDateTime timeCaptured = LocalDateTime.now();
    String userCaptured = "Administrator";
    this.setTimeCaptured(timeCaptured);
    this.setUserCaptured(userCaptured);
    this.setVersion(1);
  }

  public void setUpdateAuditInfo() {
    setTimeLastModified(LocalDateTime.now());
    setUserLastModified("Administrator");
    setVersion(this.getVersion() + 1);
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getVersion() {
    return version;
  }

  public void setVersion(int version) {
    this.version = version;
  }

  public LocalDateTime getTimeCaptured() {
    return timeCaptured;
  }

  public void setTimeCaptured(LocalDateTime timeCaptured) {
    this.timeCaptured = timeCaptured;
  }

  public LocalDateTime getTimeLastModified() {
    return timeLastModified;
  }

  public void setTimeLastModified(LocalDateTime timeLastModified) {
    this.timeLastModified = timeLastModified;
  }

  public String getUserCaptured() {
    return userCaptured;
  }

  public void setUserCaptured(String userCaptured) {
    this.userCaptured = userCaptured;
  }

  public String getUserLastModified() {
    return userLastModified;
  }

  public void setUserLastModified(String userLastModified) {
    this.userLastModified = userLastModified;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + (int) (id ^ (id >>> 32));
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BaseEntity other = (BaseEntity) obj;
    if (id != other.id)
      return false;
    return true;
  }

}
