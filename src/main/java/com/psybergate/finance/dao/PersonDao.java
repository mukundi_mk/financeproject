package com.psybergate.finance.dao;


import com.psybergate.finance.model.Person;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface PersonDao extends CrudRepository<Person,UUID> {

}
